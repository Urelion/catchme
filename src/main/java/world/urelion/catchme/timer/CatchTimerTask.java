/**
 * package of {@link java.util.Timer}s
 */
package world.urelion.catchme.timer;

// imports
// Java imports

import com.massivecraft.factions.FPlayers;
import com.massivecraft.factions.Faction;
import com.massivecraft.factions.Factions;
import com.sk89q.worldguard.LocalPlayer;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import world.urelion.catchme.CatchMe;
import world.urelion.catchme.region.CatchableArea;

import java.util.Timer;
import java.util.TimerTask;

/**
 * defines a {@link TimerTask} to wait for complete catch
 *
 * @author ChrissW-R1
 * @version 1.0
 * @since 0.9.1
 */
public class CatchTimerTask
	extends TimerTask {
	// attributes
	// normal attributes

	/**
	 * the {@link CatchableArea}, which would be caught
	 *
	 * @author ChrissW-R1
	 * @since 0.9.1
	 */
	private final CatchableArea area;
	/**
	 * would the timer started, without a delay
	 *
	 * @author ChrissW-R1
	 * @since 1.0
	 */
	private final boolean       directly;

	// constructors

	/**
	 * standard constructor
	 *
	 * @param area     the {@link CatchableArea} to change the owner to
	 * @param directly {@code true} if the timer should start without a delay
	 * @author ChrissW-R1
	 * @since 1.0
	 */
	public CatchTimerTask(CatchableArea area, boolean directly) {
		// call the constructor of the super class
		super();

		// set the attributes
		this.area     = area;
		this.directly = directly;
	}

	/**
	 * constructor, without directly
	 *
	 * @param area the {@link CatchableArea} to change the owner to
	 * @author ChrissW-R1
	 * @since 0.9.1
	 */
	public CatchTimerTask(CatchableArea area) {
		// call greater constructor with set directly to false
		this(area, false);
	}

	// methods
	// implemented methods

	/* (non-Javadoc)
	 * @see java.util.TimerTask#run()
	 */
	@Override
	public void run() {
		// check, if the region still exist
		if(this.getArea().getRegion() != null) {
			// get the name of the region
			String name = this.getArea().getName();

			// check, if the region is flagged as free for all
			if(this.getArea().isFFA()) {
				// get the current owner
				Player
					currentController =
					this.getArea().getControllingPlayer();
				// check, if a current owner exist
				if(currentController != null) {
					// get information, if the conqueror and the controller are in the same faction
					boolean sameFaction = FPlayers.getInstance().getByPlayer(
						currentController) ==
										  FPlayers.getInstance().getByPlayer(
											  this.getArea()
												  .getConquerPlayer());

					// iterate over all areas, which the area of this timer is necessary for
					for(CatchableArea area : this.getArea()
												 .getAreasNecessaryFor()) {
						// check, if a region is flagged as free for all
						if(area.isFFA()) {
							// start directly neutralization for downstream areas
							area.startNeutralizing(true);
						}
						// if the region is not flagged as free for all
						else {
							// check, if the conqueror and the controller are in the same faction
							if(!sameFaction) {
								// start directly neutralization for downstream areas
								area.startNeutralizing(true);
							}
						}
					}

					// stop the earning process of the catchable area
					this.getArea().stopEarning();

					// get the WorldGuard player
					LocalPlayer player = CatchMe.getWorldGuard().wrapPlayer(
						currentController);

					// check, if the region is not flagged as save to member
					if(!(this.getArea().isMember())) {
						// remove the current controlling owner
						this.getArea().getRegion().getOwners().removePlayer(
							player);
					}
					// if the region is flagged as save to member
					else {
						// remove the current controlling member
						this.getArea().getRegion().getMembers().removePlayer(
							player);
					}

					// check, if the region has a new owner or would be neutralize
					if(this.getArea().getConquerPlayer() != null) {
						// send a warning message to the owner
						currentController.sendMessage(
							ChatColor.RED +
							"You lost control of the region "
							+
							ChatColor.BLUE +
							name
							+
							ChatColor.RED +
							" to "
							+
							ChatColor.AQUA +
							this.getArea().getConquerPlayer().getName()
							+
							ChatColor.RED +
							"!"
						);
					}
					// if no new owner faction is set
					else {
						// create a string with information about the reason
						String reason = "";

						// check, if this is a directly neutralization
						if(this.isDirectly()) {
							// set the string with the reason
							reason = "lost control of a necessary region";
						}
						// if this is a normal neutralization
						else {
							// set the string with the reason
							reason =
								"wasn't in it as the last "
								+
								ChatColor.LIGHT_PURPLE +
								(
									this.getArea().getPeriod() *
									this.getArea().getDuration()
								)
								+
								ChatColor.YELLOW +
								" seconds";
						}

						// send a information message to the attacker
						currentController.sendMessage(
							ChatColor.YELLOW + "You lost control of the region "
							+ ChatColor.BLUE + name
							+ ChatColor.YELLOW + ", because you " + reason + "!"
						);
					}
				}

				// check, if the set conqueror is allowed to conquer the region
				if(this.getArea().getConquerPlayer() != null) {
					// get the WorldGuard player
					LocalPlayer
						player =
						CatchMe.getWorldGuard().wrapPlayer(this.getArea()
															   .getConquerPlayer());

					// check, if the region is not flagged as save to member
					if(!(this.getArea().isMember())) {
						// add the new owner
						this.getArea()
							.getRegion()
							.getOwners()
							.addPlayer(player);
					}
					// if the region is not flagged as save to member
					else {
						// add the new member
						this.getArea()
							.getRegion()
							.getMembers()
							.addPlayer(player);
					}

					// start the earning process
					this.getArea().startEarning();

					// create string with information about the formerly owner faction
					String message = "";
					// check, if this region had a formerly owner faction
					if(currentController != null) {
						// set the string, with the information about the formerly owner faction
						message =
							ChatColor.GREEN + " formerly controlled by "
							+ ChatColor.AQUA + currentController.getName();
					}

					// send an information message to all online players, of the attacker faction
					this.getArea().getConquerPlayer().sendMessage(
						ChatColor.GREEN +
						"You successfully conquered the region "
						+
						ChatColor.BLUE +
						name
						+
						message
						+
						ChatColor.GREEN +
						"."
					);
				}
			}
			// if the region is not flagged as free for all
			else {
				// get the current owner
				Faction
					currentController =
					this.getArea().getControllingFaction();

				// check, if a current owner exist
				if(currentController != null) {
					// iterate over all areas, which the area of this timer is necessary for
					for(CatchableArea area : this.getArea()
												 .getAreasNecessaryFor()) {
						// start directly neutralization for downstream areas
						area.startNeutralizing(true);
					}

					// stop the earning process of the catchable area
					this.getArea().stopEarning();

					// check, if the region is not flagged as save to member
					if(!(this.getArea().isMember())) {
						// remove the controlling owner
						this.getArea().getRegion().getOwners().removeGroup(
							currentController.getTag());
					}
					// if the region is flagged as save to member
					else {
						// remove the controlling member
						this.getArea().getRegion().getMembers().removeGroup(
							currentController.getTag());
					}

					// check, if the region has a new owner or would be neutralize
					if(this.getArea().getConquerFaction() != null) {
						// send a warning message to all online players, of the defender faction
						currentController.sendMessage(
							ChatColor.RED +
							"Your faction lost control of the region "
							+
							ChatColor.BLUE +
							name
							+
							ChatColor.RED +
							" to the faction "
							+
							ChatColor.AQUA +
							this.getArea().getConquerFaction().getTag()
							+
							ChatColor.RED +
							"!"
						);
					}
					// if no new owner faction is set
					else {
						// create a string with information about the reason
						String reason = "";

						// check, if this is a directly neutralization
						if(this.isDirectly()) {
							// set the string with the reason
							reason =
								" your faction lost control of a necessary region";
						}
						// if this is a normal neutralization
						else {
							// set the string with the reason
							reason =
								"no one of your faction was in it as the last "
								+
								ChatColor.LIGHT_PURPLE +
								(
									this.getArea().getPeriod() *
									this.getArea().getDuration()
								)
								+
								ChatColor.YELLOW +
								" seconds";
						}

						// send a information message to all online players, of the attacker faction
						currentController.sendMessage(
							ChatColor.YELLOW +
							"Your faction lost control of the region "
							+
							ChatColor.BLUE +
							name
							+
							ChatColor.YELLOW +
							", because " +
							reason +
							"!"
						);

						// iterate over all factions
						for(Faction alliedFaction : Factions.getInstance()
															.getAllFactions()) {
							// check, if the faction is allied to the controlling faction
							if(currentController.getRelationTo(alliedFaction)
												.isAlly()) {
								// send an information message to the allied faction
								alliedFaction.sendMessage(
									ChatColor.YELLOW +
									"The faction "
									+
									ChatColor.AQUA +
									this.getArea().getConquerFaction().getTag()
									+
									ChatColor.YELLOW +
									" successfully conquered the region "
									+
									ChatColor.BLUE +
									name
									+
									ChatColor.YELLOW +
									", formerly controlled by your allied faction "
									+
									ChatColor.AQUA +
									currentController.getTag()
									+
									ChatColor.YELLOW +
									"!"
								);
							}
						}
					}
				}

				// check, if the set conqueror is allowed to conquer the region
				if(this.getArea().isAllowedToConquer(this.getArea()
														 .getConquerFaction())) {
					// get the tag of the conquer faction
					String
						conquerFactionTag =
						this.getArea().getConquerFaction().getTag();

					// check, if the region is not flagged as save to member
					if(!(this.getArea().isMember())) {
						// add the new owner
						this.getArea().getRegion().getOwners().addGroup(
							conquerFactionTag);
					}
					// if the region is flagged as save to member
					else {
						// add the new member
						this.getArea().getRegion().getMembers().addGroup(
							conquerFactionTag);
					}

					// start the earning process
					this.getArea().startEarning();

					// create string with information about the formerly owner faction
					String message = "";
					// check, if this region had a formerly owner faction
					if(currentController != null) {
						// set the string, with the information about the formerly owner faction
						message =
							ChatColor.GREEN +
							" formerly controlled by the faction "
							+
							ChatColor.AQUA +
							currentController.getTag();
					}

					// send an information message to all online players, of the attacker faction
					this.getArea().getConquerFaction().sendMessage(
						ChatColor.GREEN +
						"Your faction successfully conquered the region "
						+
						ChatColor.BLUE +
						name
						+
						message
						+
						ChatColor.GREEN +
						"."
					);
				}
			}
		}

		// stop the catching process
		this.getArea().stopCatching();
	}

	// getters and setters

	/**
	 * gives the {@link CatchableArea}, which would be caught
	 *
	 * @return the {@link CatchableArea}, which would be caught
	 * @author ChrissW-R1
	 * @since 0.9.1
	 */
	public CatchableArea getArea() {
		return this.area;
	}

	/**
	 * has the {@link Timer} no delay?
	 *
	 * @return <code>true</code>, if the {@link Timer} has no delay
	 * @author ChrissW-R1
	 * @since 1.0
	 */
	public boolean isDirectly() {
		return this.directly;
	}
}
