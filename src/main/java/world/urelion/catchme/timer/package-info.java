/**
 * package of the {@link java.util.Timer}s
 * 
 * @author ChrissW-R1
 * @version 1.0
 * @since 0.9.1
 */
package world.urelion.catchme.timer;
