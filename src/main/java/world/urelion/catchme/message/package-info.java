/**
 * package of {@link world.urelion.catchme.message.Message} handling
 * 
 * @author ChrissW-R1
 * @version 0.9.1
 * @since 0.9.1
 */
package world.urelion.catchme.message;
