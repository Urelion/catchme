/**
 * package of {@link Message} handling
 */
package world.urelion.catchme.message;

// imports
// Java imports

import java.util.Iterator;

// external imports

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.getspout.spout.Spout;
import org.getspout.spoutapi.SpoutManager;
import world.urelion.catchme.CatchMe;

import com.massivecraft.factions.FPlayer;
import com.massivecraft.factions.Faction;

/**
 * defines a {@link Message}, which could be send to {@link Player}s
 * 
 * @author ChrissW-R1
 * @version 1.0
 * @since 0.9.1
 */
public class Message
{
	// attributes
	// class attributes
	
	/**
	 * the default {@link Material} of the message window
	 * @author ChrissW-R1
	 * @since 0.9.1
	 */
	public static final Material DEFAULT_MATERIAL = Material.AIR;
	/**
	 * the length of {@link ChatColor} codes
	 * @author ChrissW-R1
	 * @since 0.9.1
	 */
	public static final int CHATCOLOR_LENGTH = ChatColor.WHITE.toString().length();
	/**
	 * the length of {@link String}s in message windows of {@link Spout}
	 * @author ChrissW-R1
	 * @since 0.9.1
	 */
	public static final int NOTIFICATION_LENGTH = 26;
	
	// normal attributes
	
	/**
	 * the title of the message window
	 * @author ChrissW-R1
	 * @since 0.9.1
	 */
	private final String title;
	/**
	 * should be ignore the spaces in the title
	 * @author ChrissW-R1
	 * @since 0.9.1
	 */
	private final boolean ignoreTitleSpaces;
	/**
	 * the {@link Message} of the message window
	 * @author ChrissW-R1
	 * @since 0.9.1
	 */
	private final String message;
	/**
	 * should be ignore the spaces in the message
	 * @author ChrissW-R1
	 * @since
	 */
	private final boolean ignoreMessageSpaces;
	/**
	 * the {@link Material} of the message window
	 * @author ChrissW-R1
	 * @since 0.9.1
	 */
	private final Material material;
	/**
	 * the alternative {@link Message} to show, if no message window
	 * @author ChrissW-R1
	 * @since 0.9.1
	 */
	private final String altMessage;
	
	// constructors
	
	/**
	 * standard constructor
	 * @author ChrissW-R1
	 * @since 0.9.1
	 * 
	 * @param title the title of the message window
	 * @param ignoreTitleSpaces should be ignore the spaces in the title
	 * @param message the {@link Message} in the message window
	 * @param ignoreMessageSpaces should be ignore the spaces in the message
	 * @param material the {@link Material} of the message window
	 * @param altMessage the alternative {@link Message}, if no message window couldn't be created
	 */
	public Message(String title, boolean ignoreTitleSpaces, String message, boolean ignoreMessageSpaces, Material material, String altMessage)
	{
		// call the constructor of the super class
		super();
		
		// set the attributes
		this.title = title;
		this.ignoreTitleSpaces = ignoreTitleSpaces;
		this.message = message;
		this.ignoreMessageSpaces = ignoreMessageSpaces;
		this.material = material;
		this.altMessage = altMessage;
	}
	
	/**
	 * constructor, without ignoring spaces
	 * @author ChrissW-R1
	 * @since 0.9.1
	 * 
	 * @param title the title of the message window
	 * @param message the {@link Message} in the message window
	 * @param material the {@link Material} of the message window
	 * @param altMessage the alternative {@link Message}, if no message window couldn't be created
	 * @see Message#Message(String, boolean, String, boolean, Material, String)
	 */
	public Message(String title, String message, Material material, String altMessage)
	{
		// call greater constructor with ignoring spaces
		this(title, false, message, false, material, altMessage);
	}

	/**
	 * constructor, without a {@link Material}
	 * @author ChrissW-R1
	 * @since 0.9.1
	 * 
	 * @param title the title of the message window
	 * @param message the {@link Message} of the message window
	 * @param altMessage the alternative {@link Message}, if no message window couldn't be created
	 * @see Message#Message(String, String, Material, String)
	 */
	public Message(String title, String message, String altMessage)
	{
		// call greater constructor with the default material
		this(title, message, Message.DEFAULT_MATERIAL, altMessage);
	}
	
	/**
	 * constructor, without an alternative {@link Message}
	 * @author ChrissW-R1
	 * @since 0.9.1
	 * 
	 * @param title the title of the message window
	 * @param message the {@link Message} of the message window
	 * @see Message#Message(String, String, String)
	 */
	public Message(String title, String message)
	{
		// call greater constructor with the message also as the alternative message
		this(title, message, message);
	}
	
	// methods
	// getters and setters
	
	/**
	 * gives the title of the message window
	 * @author ChrissW-R1
	 * @since 0.9.1
	 * 
	 * @return the title of the message window
	 */
	public String getTitle()
	{
		return this.title;
	}

	/**
	 * should be ignore the spaces in the title
	 * @author ChrissW-R1
	 * @since 0.9.1
	 * 
	 * @return should be ignore the spaces in the title
	 */
	public boolean isIgnoreTitleSpaces()
	{
		return this.ignoreTitleSpaces;
	}

	/**
	 * gives the {@link Message} in the message window
	 * @author ChrissW-R1
	 * @since 0.9.1
	 * 
	 * @return the {@link Message} in the message window
	 */
	public String getMessage()
	{
		return this.message;
	}

	/**
	 * should be ignore the spaces in the message
	 * @author ChrissW-R1
	 * @since 0.9.1
	 * 
	 * @return should be ignore the spaces in the message
	 */
	public boolean isIgnoreMessageSpaces()
	{
		return this.ignoreMessageSpaces;
	}

	/**
	 * gives the {@link Material} of the message window
	 * @author ChrissW-R1
	 * @since 0.9.1
	 * 
	 * @return the {@link Material} of the message window
	 */
	public Material getMaterial()
	{
		return this.material;
	}

	/**
	 * gives the alternative {@link Message}, if no message window could be created
	 * @author ChrissW-R1
	 * @since 0.9.1
	 * 
	 * @return the alternative {@link Message}
	 */
	public String getAltMessage()
	{
		return this.altMessage;
	}
	
	// normal methods
	
	/**
	 * gives the count of {@link ChatColor}s in a {@link String}
	 * @author ChrissW-R1
	 * @since 0.9.1
	 * 
	 * @param string the {@link String} to count in
	 * @return the count of {@link ChatColor}s in the {@link String}
	 */
	public static int countColor(String string)
	{
		// create result variable
		int colorCounter = 0;
		
		// get the first index of a chat color code
		int index = string.indexOf(ChatColor.COLOR_CHAR, 0);
		// count, while a color code was found
		while(index >= 0)
		{
			// check, if the code really represent a color
			if(ChatColor.getByChar(string.charAt(index)).isColor())
			{
				// count
				colorCounter++;
			}
			
			// set the new index
			index = string.indexOf(ChatColor.COLOR_CHAR, index + Message.CHATCOLOR_LENGTH);
		}
		
		// return the count of color codes
		return colorCounter;
	}
	
	/**
	 * removes double {@link ChatColor}s in a {@link String}<br />
	 * <code>&amp;6&amp;a</code> means {@link ChatColor#GOLD} and {@link ChatColor#GREEN}, but nothing character is colored as {@link ChatColor#GOLD}, so it would be removed
	 * @author ChrissW-R1
	 * @since 0.9.1
	 * 
	 * @param string the {@link String} to clean
	 * @return the {@link String} without double {@link ChatColor}
	 */
	public static String removeDoubleColors(String string)
	{
		// create an index variable
		int index = string.indexOf(ChatColor.COLOR_CHAR, 0);
		
		// check, if another color followed
		while(index >= 0)
		{
			// get the string after the color code
			String toEnd = string.substring(index + Message.CHATCOLOR_LENGTH);
			
			// check, if the followed string begins with a another color code
			if(toEnd.startsWith("" + ChatColor.COLOR_CHAR))
			{
				// check if both codes are colors
				if(
						ChatColor.getByChar(string.charAt(index + 1)).isColor()
						&& ChatColor.getByChar(toEnd.charAt(1)).isColor()
				)
				{
					// get the new string without the first color code, because it is useless
					string = string.substring(0, index) + toEnd;
				}
			}
			
			// set the index to the next possible position
			index = string.indexOf(ChatColor.COLOR_CHAR, index + Message.CHATCOLOR_LENGTH);
		}
		
		// return the cleaned string
		return string;
	}
	
	/**
	 * gives a reduced {@link String}, broken at a given length and with <code>...</code> at the end<br />
	 * Examples:<br />
	 * <ul>
	 * <li>{@link String}: <code>Hello World</code>, length: <code>6</code> &rArr; <code>Hel...</code></li>
	 * <li>{@link String}: <code>Hello World</code>, length: <code>10</code> &rArr; <code>Hello...</code><br />
	 * Here the method uses the space to break. (Only, if ignoreSpace is not set to <code>true</code>.)</li>
	 * <li>{@link String}: <code>Hello World</code>, length: <code>11</code> &rArr; <code>Hello World</code><br />
	 * Here the method does nothing, because the string is already short enough.</li>
	 * </ul>
	 * 
	 * @author ChrissW-R1
	 * @since 0.9.1
	 * 
	 * @param string the {@link String} to reduce
	 * @param length the maximum length of the {@link String}, without {@link ChatColor}s
	 * @param ignoreSpace set to <code>true</code>, if you don't want to break on a space
	 * @return the reduced string in the given length (without {@link ChatColor}s)
	 */
	public static String getReducedString(String string, int length, boolean ignoreSpace)
	{
		// create result variable without double colors
		String reducedString = Message.removeDoubleColors(string);
		
		// check, if the string ends with a color code
		while(reducedString.substring(Message.CHATCOLOR_LENGTH).startsWith("" +ChatColor.COLOR_CHAR))
		{
			// remove the color code at the end of the string
			reducedString = reducedString.substring(0, reducedString.length() - Message.CHATCOLOR_LENGTH);
		}
		
		// get the string without any color
		String colorlessString = ChatColor.stripColor(reducedString);
		
		// check, if the string is too long
		if(colorlessString.length() > length)
		{
			// set the index, where to break the string, to the maximum length minus space for ...
			int breakIndex = length - 3;
			
			// check, if the spaces should be ignored
			if(!ignoreSpace)
			{
				// get the index of the last space in the possible zone
				int spaceIndex = colorlessString.substring(0, breakIndex).lastIndexOf(" ");
				// check, if any space in the possible zone of the string was found
				if(spaceIndex >= 0)
				{
					// set the index to break to the index of the last space in the zone
					breakIndex = spaceIndex;
				}
			}
			
			// create a counter of all color codes, which are in the reduced string
			int colorCounter = 0;
			// check, if another color code is in the reduced zone
			while(!((colorlessString.substring(0, breakIndex)).equals(ChatColor.stripColor(reducedString.substring(0, breakIndex + (Message.CHATCOLOR_LENGTH * colorCounter))))))
			{
				// count
				colorCounter++;
			}
			
			// reduce the string at the break index and at the ...
			reducedString = reducedString.substring(0, breakIndex + (Message.CHATCOLOR_LENGTH * colorCounter)) + "...";
		}
		
		// return the reduced string
		return reducedString;
	}
	
	/**
	 * gives a reduced {@link String}, without ignoring the spaces
	 * @author ChrissW-R1
	 * @since 0.9.1
	 * 
	 * @param string the {@link String} to reduce
	 * @param length the maximum length of the {@link String}, without {@link ChatColor}s
	 * @return the reduced string in the given length (without {@link ChatColor}s)
	 * @see Message#getReducedString(String, int, boolean)
	 */
	public static String getReducedString(String string, int length)
	{
		// return the reduced string, without ignore the spaces
		return Message.getReducedString(string, length, false);
	}
	
	/**
	 * send the {@link Message} to a {@link Player}
	 * @author ChrissW-R1
	 * @since 0.9.1
	 * 
	 * @param player the {@link Player} to send the {@link Message} to
	 * @return <code>true</code>, if the {@link Spout} {@link Plugin} is installed and the {@link Player} used the Spoutcraft client
	 */
	public boolean send(Player player)
	{
		// check, if the spout plugin is installed
		if(
				CatchMe.getSpout() != null
				&& SpoutManager.getPlayer(player).isSpoutCraftEnabled()
		)
		{
			// show the message in a message window of the Spoutcraft Client
			SpoutManager.getPlayer(player).sendNotification(
					Message.getReducedString(this.getTitle(), Message.NOTIFICATION_LENGTH, this.isIgnoreTitleSpaces()),
					Message.getReducedString(this.getMessage(), Message.NOTIFICATION_LENGTH, this.isIgnoreMessageSpaces()),
					this.getMaterial()
			);
			// return the information, that the spout plugin is installed and the player used the Spoutcraft client
			return true;
		}
		
		// send the alternative message to the player, via the chat
		player.sendMessage(Message.removeDoubleColors(this.getAltMessage()));
		
		// return the information, that the spout plugin is not installed
		return false;
	}
	
	/**
	 * send the {@link Message} to all online {@link Player}s of a {@link Faction}
	 * @author ChrissW-R1
	 * @since 0.9.1
	 * 
	 * @param faction the {@link Faction}, to send the {@link Message} to
	 * @return <code>true</code>, if the {@link Spout} {@link Plugin} is installed
	 */
	public boolean send(Faction faction)
	{
		// iterate over all online players of the faction
		Iterator<FPlayer> iter = faction.getFPlayersWhereOnline(true).iterator();
		while(iter.hasNext())
		{
			// send the message to the player
			this.send(iter.next().getPlayer());
		}
		
		// return true, if the spout plugin is installed
		if(CatchMe.getSpout() != null)
			return true;
		
		// return false, if the spout plugin is not installed
		return false;
	}
}
