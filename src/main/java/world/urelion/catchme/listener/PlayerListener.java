/**
 * package of {@link Listener}
 */
package world.urelion.catchme.listener;

// imports
// external imports

import org.bukkit.Server;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;

// project imports

import world.urelion.catchme.region.AreaHandler;

/**
 * defines a class
 * 
 * @author ChrissW-R1
 * @version 1.0
 * @since 0.9.1
 */
public class PlayerListener
implements Listener
{
	// constructors
	
	/**
	 * standard constructor
	 * @author ChrissW-R1
	 * @since 0.9.1
	 */
	public PlayerListener()
	{
		// call the constructor of the super class
		super();
	}
	
	// methods
	// normal methods
	
	/**
	 * interacts on {@link Player} movement
	 * @author ChrissW-R1
	 * @since 0.9.1
	 * 
	 * @param event the {@link PlayerMoveEvent}
	 */
	@EventHandler
	public void onPlayerMove(PlayerMoveEvent event)
	{
		// check, if the event is already canceled
		if(event.isCancelled())
		{
			// break event process of CatchMe
			return;
		}
		
		// check, if the player changed its position
		if(!(event.getFrom().equals(event.getTo())))
		{
			// start lookup process
			AreaHandler.playerCatchingLookup(event);
		}
	}
	
	/**
	 * interacts, if a {@link Player} joined the {@link Server}
	 * @author ChrissW-R1
	 * @since 0.9.1
	 * 
	 * @param event the {@link PlayerJoinEvent}
	 */
	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent event)
	{
		// start lookup process for spawning in a catchable region
		AreaHandler.playerCatchingLookup(event);
	}
	
	/**
	 * interacts, if a {@link Player} left the {@link Server}
	 * @author ChrissW-R1
	 * @since 0.9.1
	 * 
	 * @param event the {@link PlayerQuitEvent}
	 */
	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent event)
	{
		// start lookup process for leaving the server, while standing in a catchable region
		AreaHandler.playerQuitArea(event);
	}
}
