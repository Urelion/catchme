/**
 * package of the {@link ProtectedRegion} handling
 */
package world.urelion.catchme.region;

// imports
// Java imports

import com.massivecraft.factions.FPlayers;
import com.massivecraft.factions.Faction;
import com.massivecraft.factions.Factions;
import com.massivecraft.factions.integration.Econ;
import com.massivecraft.factions.perms.Relation;
import com.sk89q.worldedit.bukkit.BukkitAdapter;
import com.sk89q.worldguard.WorldGuard;
import com.sk89q.worldguard.protection.flags.*;
import com.sk89q.worldguard.protection.flags.StateFlag.State;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.Player;
import world.urelion.catchme.CatchMe;
import world.urelion.catchme.timer.CatchTimerTask;

import java.util.*;

/**
 * defines a catchable {@link ProtectedRegion}
 *
 * @author ChrissW-R1
 * @version 1.1
 * @since 0.9.1
 */
public class CatchableArea {
	// attributes
	// class attributes

	/**
	 * the default time in seconds to capture the area
	 *
	 * @author ChrissW-R1
	 * @since 0.9.1
	 */
	public static final int    DEFAULT_CATCHINGTIME = 60 * 15;
	/**
	 * the default period in seconds and the delay between each earn money
	 * <code>0</code> &rArr; disable earning money
	 *
	 * @author ChrissW-R1
	 * @since 0.9.1
	 */
	public static final int
							   DEFAULT_PERIOD       =
		CatchableArea.DEFAULT_CATCHINGTIME * 4;
	/**
	 * default number of periods before the area becomes neutral<br />
	 * <code>0</code> &rArr; never become neutral
	 *
	 * @author ChrissW-R1
	 * @since 0.9.1
	 */
	public static final int    DEFAULT_DURATION     = 0;
	/**
	 * money that the owner earns each period
	 *
	 * @author ChrissW-R1
	 * @since 0.9.1
	 */
	public static final double DEFAULT_MONEY        = 50;

	// normal attributes

	/**
	 * the {@link ProtectedRegion}, which should be catchable
	 *
	 * @author ChrissW-R1
	 * @since 0.9.1
	 */
	private ProtectedRegion region;
	/**
	 * the {@link World}, in which the {@link ProtectedRegion} is
	 *
	 * @author ChrissW-R1
	 * @since 0.9.1
	 */
	private World           world;

	/**
	 * list of all {@link Player}s, which are currently in this {@link
	 * ProtectedRegion}
	 *
	 * @author ChrissW-R1
	 * @since 0.9.1
	 */
	private ArrayList<Player> players        = new ArrayList<Player>();
	/**
	 * the {@link Timer} to wait until the catching time is over
	 *
	 * @author ChrissW-R1
	 * @since 0.9.1
	 */
	private Timer             catchingTimer  = null;
	/**
	 * the {@link Timer} to earning money for this {@link ProtectedRegion}
	 *
	 * @author ChrissW-R1
	 * @since 0.9.1
	 */
	private Timer             earningTimer   = null;
	/**
	 * the {@link Faction}, which is currently occupy this {@link
	 * ProtectedRegion}
	 *
	 * @author ChrissW-R1
	 * @since 0.9.1
	 */
	private Faction           conquerFaction = null;
	/**
	 * the {@link Player}, which is currently occupy this {@link
	 * ProtectedRegion}<br /> only for free-for-all-modus
	 *
	 * @author ChrissW-R1
	 * @since 1.0
	 */
	private Player            conquerPlayer  = null;

	// constructors

	/**
	 * standard constructor
	 *
	 * @param region the {@link ProtectedRegion}
	 * @param world  the {@link World}, in which the {@link ProtectedRegion} is
	 * @author ChrissW-R1
	 * @since 0.9.1
	 */
	protected CatchableArea(ProtectedRegion region, World world) {
		// call the constructor of the super class
		super();

		// check, if the region is catchable
		if(!(AreaHandler.isRegionCatchable(region))) {
			// set the Region to catchable
			region.setFlag(
				(StateFlag) (CatchMe.getFlags().get("catchable")),
				AreaHandler.booleanToState(true)
			);
		}

		// set the region
		this.setRegion(region, world);

		// check, if the region is controlled by a faction
		if(this.getControllingFaction() != null) {
			// start the timer of earning money
			this.startEarning();

			// check, if a defender is in this region
			if(!(this.isDefenderHere()) && !(this.isCatching())) {
				// start neutralization
				this.startNeutralizing(false);
			}
		}
	}

	// methods
	// implemented methods

	@Override
	public int hashCode() {
		return Objects.hash(region, world);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		// check, if the other object is a CatchableArea
		if(obj instanceof CatchableArea) {
			// cast the other object into a CatchableArea
			CatchableArea another = (CatchableArea) obj;
			// return, if both regions, are the same
			return (
				this.getWorld() == another.getWorld()
				&& this.getRegion() != null
				&& this.getRegion().equals(another.getRegion())
			);
		}

		// return false, if the other object is not a CatchableArea
		return false;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#finalize()
	 */
	@Override
	protected void finalize()
	throws Throwable {
		// stop any active process
		this.stopAll();

		// call method from super class
		super.finalize();
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return
			CatchableArea.class.getName()
			+ "[World: " + this.getWorld().getName()
			+ ", Region: " + this.getName()
			+ "]";
	}

	// getters and setters

	/**
	 * gives the {@link ProtectedRegion}
	 *
	 * @return the {@link ProtectedRegion}
	 * @author ChrissW-R1
	 * @since 0.9.1
	 */
	public ProtectedRegion getRegion() {
		return this.region;
	}

	/**
	 * gives the {@link World}, in which the {@link ProtectedRegion} is
	 *
	 * @return the {@link World}, in which the {@link ProtectedRegion} is
	 * @author ChrissW-R1
	 * @since 0.9.1
	 */
	public World getWorld() {
		return this.world;
	}

	/**
	 * sets the {@link ProtectedRegion}
	 *
	 * @param region the {@link ProtectedRegion} to set
	 * @param world  the {@link World}, in which the {@link ProtectedRegion} is
	 * @author ChrissW-R1
	 * @since 0.9.1
	 */
	public void setRegion(ProtectedRegion region, World world) {
		this.region = region;
		this.world  = world;
	}

	/**
	 * gives a list of all {@link Player}s, which are currently in this {@link
	 * ProtectedRegion}
	 *
	 * @return the list of all {@link Player}s, which are currently in this
	 * {@link ProtectedRegion}
	 * @author ChrissW-R1
	 * @since 0.9.1
	 */
	public ArrayList<Player> getPlayers() {
		return this.players;
	}

	/**
	 * gives the {@link Faction}, which is currently occupy this {@link
	 * ProtectedRegion}
	 *
	 * @return the {@link Faction}, which is currently occupy this {@link
	 * ProtectedRegion}
	 * @author ChrissW-R1
	 * @since 0.9.1
	 */
	public Faction getConquerFaction() {
		return this.conquerFaction;
	}

	/**
	 * gives the {@link Player}, which is currently occupy this {@link
	 * ProtectedRegion}
	 *
	 * @return the {@link Player}, which is currently occupy this {@link
	 * ProtectedRegion}
	 * @author ChrissW-R1
	 * @since 1.0
	 */
	public Player getConquerPlayer() {
		return this.conquerPlayer;
	}

	// normal methods

	/**
	 * gives a list with group names from a list with {@link Faction}s
	 *
	 * @param factionList the list with {@link Faction}s
	 * @return the list with group names
	 * @author ChrissW-R1
	 * @since 0.9.1
	 */
	private static List<String> getFactionStringList(List<Faction> factionList) {
		List<String> stringList = new ArrayList<>();

		// iterate over all factions
		for(Faction faction : factionList) {
			stringList.add(faction.getTag());
		}

		return stringList;
	}

	/**
	 * start the earning process
	 *
	 * @author ChrissW-R1
	 * @since 0.9.1
	 */
	public void startEarning() {
		// creates a new timer of earning money
		this.earningTimer = new Timer();
		// set the task and the period and start the timer
		this.earningTimer.schedule(new TimerTask() {
			/*
			 * (non-Javadoc)
			 * @see java.util.TimerTask#run()
			 */
			@Override
			public void run() {
				// get the owner of the region
				Object controller = CatchableArea.this.getController();
				// check, if this region is owned by someone
				if(controller != null) {
					// get the money to earn
					double money = CatchableArea.this.getMoney();

					// check, if the owner is a faction
					if(controller instanceof Faction) {
						// send money to the faction
						Econ.modifyBalance(
							((Faction) controller).getAccountId(),
							money
						);
					}
					// check, if the owner is a player
					else if(controller instanceof Player) {
						// check, if the earning value is positive
						if(money > 0) {
							// send money to the player
							CatchMe.getEconomy().depositPlayer(
								((Player) controller).getName(),
								money
							);
						}
						// check, if the earning value is negative
						else if(money < 0) {
							// take the money from the player
							CatchMe.getEconomy().withdrawPlayer(
								((Player) controller).getName(),
								money * (-1)
							);
						}
					}

					// restart the timer of earning money
					CatchableArea.this.startEarning();
				}
				// if no faction owns this region
				else {
					// remove the timer of earning money
					CatchableArea.this.stopEarning();
				}
			}
		}, this.getPeriod() * 1000);
	}

	/**
	 * stop the earning process
	 *
	 * @author ChrissW-R1
	 * @since 0.9.1
	 */
	public void stopEarning() {
		// check, if a timer of earning money is active
		if(this.earningTimer != null) {
			// cancel the timer process
			this.earningTimer.cancel();
			// remove the timer
			this.earningTimer = null;
		}
	}

	/**
	 * starts the {@link Timer} to catch this {@link ProtectedRegion}
	 *
	 * @param conqueror the {@link Faction}, which current occupy this {@link
	 *                  ProtectedRegion}
	 * @author ChrissW-R1
	 * @see CatchableArea#startCatching(Player)
	 * @since 0.9.1
	 */
	private void startCatching(Faction conqueror) {
		// check, if a timer is enabled
		if(this.catchingTimer != null) {
			// cancel the timer process
			this.catchingTimer.cancel();
		}

		// reset the conquer player
		this.conquerPlayer = null;
		// set the current conqueror
		this.conquerFaction = conqueror;
		// create a new catchingTimer for this region
		this.catchingTimer = new Timer(true);

		// set the scheduler to run the catching timer and schedule the action to change the owner
		this.catchingTimer.schedule(
			new CatchTimerTask(this),
			this.getCatchingTime() * 1000
		);

		// get the name of this region
		String name = this.getName();

		// send an information message all online players of the conquer faction
		conqueror.sendMessage(
			ChatColor.YELLOW + "Your faction tries to conquer the region "
			+ ChatColor.BLUE + name
			+ ChatColor.YELLOW + "!"
		);

		// get the controlling faction
		Faction controller = this.getControllingFaction();
		// check, if any faction controls this region
		if(controller != null) {
			// send a warning message to all online players of the currently controlling faction
			controller.sendMessage(
				ChatColor.RED + "The faction "
				+ ChatColor.AQUA + conqueror.getTag()
				+ ChatColor.RED + " tries to conquer your region "
				+ ChatColor.BLUE + name
				+ ChatColor.RED + "!"
			);

			// iterate over all factions
			for(Faction faction : Factions.getInstance().getAllFactions()) {
				// check, if the faction is allied to the controlling faction
				if(controller.getRelationTo(faction).isAlly()) {
					// send a warning message to the allied faction
					faction.sendMessage(
						ChatColor.RED +
						"The faction "
						+
						ChatColor.AQUA +
						conqueror.getTag()
						+
						ChatColor.RED +
						" tries to conquer the region "
						+
						ChatColor.BLUE +
						name
						+
						ChatColor.RED +
						", which is controlled by your allied "
						+
						ChatColor.AQUA +
						controller.getTag()
						+
						ChatColor.RED +
						"!"
					);
				}
			}
		}
	}

	/**
	 * starts the {@link Timer} to catch this {@link ProtectedRegion} by a
	 * {@link Player}
	 *
	 * @param conqueror the {@link Player}, which current occupy this {@link
	 *                  ProtectedRegion}
	 * @author ChrissW-R1
	 * @see CatchableArea#startCatching(Faction)
	 * @since 1.0
	 */
	private void startCatching(Player conqueror) {
		// check, if a timer is enabled
		if(this.catchingTimer != null) {
			// cancel the timer process
			this.catchingTimer.cancel();
		}

		// reset the conquer faction
		this.conquerFaction = null;
		// set the current conqueror
		this.conquerPlayer = conqueror;
		// create a new catchingTimer for this region
		this.catchingTimer = new Timer(true);

		// set the scheduler to run the catching timer and schedule the action to change the owner
		this.catchingTimer.schedule(
			new CatchTimerTask(this),
			this.getCatchingTime() * 1000
		);

		// get the name of this region
		String name = this.getName();

		// send an information message all online players of the conquer faction
		conqueror.sendMessage(
			ChatColor.YELLOW + "You tries to conquer the region "
			+ ChatColor.BLUE + name
			+ ChatColor.YELLOW + "!"
		);

		// get the player, which controls this region
		Player controllingPlayer = this.getControllingPlayer();

		// check, if any faction controls this region
		if(controllingPlayer != null) {
			// send a warning message to all online players of the currently controlling faction
			controllingPlayer.sendMessage(
				ChatColor.AQUA + conqueror.getName()
				+ ChatColor.RED + " tries to conquer your region "
				+ ChatColor.BLUE + name
				+ ChatColor.RED + "!"
			);
		}
	}

	/**
	 * start the process to neutralize this {@link ProtectedRegion}
	 *
	 * @param directly set to <code>true</code> to proceed the neutralization
	 *                 without a delay
	 * @author ChrissW-R1
	 * @since 1.0
	 */
	public void startNeutralizing(boolean directly) {
		// set the time to a directly neutralization
		int time = 0;

		// check, if the neutralization should be directly proceed
		if(!directly) {
			// get the duration of this region
			int duration = this.getDuration();

			// break neutralization, if the duration was set to 0
			if(duration <= 0) {
				return;
			}

			// set the time to the normal neutralization value
			time = this.getPeriod() * duration * 1000;
		}

		// create a new catchingTimer for this region
		this.catchingTimer = new Timer(true);
		// set the scheduler to run the catching timer and schedule the neutralization
		this.catchingTimer.schedule(new CatchTimerTask(this, directly), time);
	}

	/**
	 * stop the {@link Timer} to catch this {@link ProtectedRegion}
	 *
	 * @author ChrissW-R1
	 * @since 0.9.1
	 */
	public void stopCatching() {
		// check, if the catching timer is set
		if(this.catchingTimer != null) {
			// cancel the timer process
			this.catchingTimer.cancel();
			// remove the timer
			this.catchingTimer = null;
		}

		// remove the conquer faction
		this.conquerFaction = null;
		// remove the conquer player
		this.conquerPlayer = null;
	}

	/**
	 * is someone trying to catch this {@link ProtectedRegion} active?
	 *
	 * @return <code>true</code>, if someone tries to catch this {@link
	 * ProtectedRegion}
	 * @author ChrissW-R1
	 * @since 0.9.1
	 */
	public boolean isCatching() {
		return (
			(
				this.getConquerFaction() != null
				|| this.getConquerPlayer() != null
			)
			&& this.catchingTimer != null
		);
	}

	/**
	 * stops any active process of this {@link CatchableArea}
	 *
	 * @author ChrissW-R1
	 * @since 1.0
	 */
	public void stopAll() {
		// stop the earning process
		this.stopEarning();
		// stop the catching process
		this.stopCatching();
	}

	/**
	 * is a {@link Flag} set for this {@link ProtectedRegion}
	 *
	 * @param flag the {@link Flag} to check
	 * @return <code>true</code>, if the {@link Flag} is set
	 * @author ChrissW-R1
	 * @since 0.9.1
	 */
	public boolean isFlagSet(Flag<?> flag) {
		// check, if the region still exist
		if(this.getRegion() != null) {
			// return, if the flag is set
			return this.getRegion().getFlags().containsKey(flag);
		}
		// if the region doesn't exist any longer
		else {
			// stop all active process
			this.stopAll();
		}

		// return false, because a non-exist region haven't any flag
		return false;
	}

	/**
	 * gives the name of this {@link ProtectedRegion} to show
	 *
	 * @return the name to show or the region id, if it is not set<br /> or
	 * <code>NO_REGION</code>, if this {@link CatchableArea} is not a valid
	 * {@link ProtectedRegion}
	 * @author ChrissW-R1
	 * @since 0.9.1
	 */
	public String getName() {
		// check, if the region still exist
		if(this.getRegion() != null) {
			Map<String, Flag<?>> flags = CatchMe.getFlags();

			// return the name to show, if it is set
			if(this.isFlagSet(flags.get("showname"))) {
				return this.getRegion().getFlag((StringFlag) (
					flags.get("showname")
				));
			}

			// return the id of the region, if no name to show is set
			return this.getRegion().getId();
		}

		// stop any active process
		this.stopAll();
		// return a default string, because the region doesn't exist any longer
		return "NO_REGION";
	}

	/**
	 * set the name of this {@link ProtectedRegion} to show
	 *
	 * @param showname the name to set
	 * @author ChrissW-R1
	 * @since 1.0
	 */
	public void setName(String showname) {
		// check, if the region still exist
		if(this.getRegion() != null) {
			// set the flag
			this.region.setFlag(
				(StringFlag) (CatchMe.getFlags().get("showname")),
				showname
			);
		}
		// if the region doesn't exist any longer
		else {
			// stop any active process
			this.stopAll();
		}
	}

	/**
	 * is this {@link ProtectedRegion} flagged as free for all?
	 *
	 * @return if this {@link ProtectedRegion} is flagged as free for all
	 * @author ChrissW-R1
	 * @since 1.0
	 */
	public boolean isFFA() {
		// get the flag
		StateFlag flag = (StateFlag) CatchMe.getFlags().get("catch-ffa");

		// check, if the flag is set
		if(this.isFlagSet(flag)) {
			// return, if the flag is set to true
			return this.getRegion().getFlag(flag) == State.ALLOW;
		}

		// return the default value of the flag
		return flag.getDefault() == State.ALLOW;
	}

	/**
	 * sets the free for all flag
	 *
	 * @param ffa the flag value to set
	 * @author ChrissW-R1
	 * @since 1.0
	 */
	public void setFFA(boolean ffa) {
		// check, if the region still exist
		if(this.getRegion() != null) {
			// set the flag
			this.region.setFlag(
				(StateFlag) (CatchMe.getFlags().get("catch-ffa")),
				AreaHandler.booleanToState(ffa)
			);
		}
		// is the region doesn't exist any longer
		else {
			// stop any active process
			this.stopAll();
		}
	}

	/**
	 * is this {@link ProtectedRegion} flagged as save to member
	 *
	 * @return if this {@link ProtectedRegion} is flagged as save to member
	 * @author ChrissW-R1
	 * @since 1.1
	 */
	public boolean isMember() {
		// get the flag
		StateFlag flag = (StateFlag) CatchMe.getFlags().get("catch-member");

		// check, if the flag is set
		if(this.isFlagSet(flag)) {
			// return, if the flag is set to true
			return this.getRegion().getFlag(flag) == State.ALLOW;
		}

		// return the default value of the flag
		return flag.getDefault() == State.ALLOW;
	}

	/**
	 * set the member flag
	 *
	 * @param member {@code true} if the conquerors should saved as members,
	 *               {@code false} for owners
	 * @author ChrissW-R1
	 * @since 1.1
	 */
	public void setMember(boolean member) {
		// check, if the region still exist
		if(this.getRegion() != null) {
			// set the flag
			this.region.setFlag(
				(StateFlag) (CatchMe.getFlags().get("catch-member")),
				AreaHandler.booleanToState(member)
			);
		}
		// is the region doesn't exist any longer
		else {
			// stop any active process
			this.stopAll();
		}
	}

	/**
	 * gives the list of {@link CatchableArea}s, which might be also conquered
	 * to conquer this {@link CatchableArea}
	 *
	 * @return the list of {@link CatchableArea}s, which might be also conquered
	 * @author ChrissW-R1
	 * @since 1.0
	 */
	public ArrayList<CatchableArea> getNecessaryAreas() {
		// create new list of all regions, which are necessary to conquer this region
		ArrayList<CatchableArea> regions = new ArrayList<CatchableArea>();

		Map<String, Flag<?>> flags = CatchMe.getFlags();

		// check, if the flag with the necessary regions is set
		if(this.isFlagSet(flags.get("necessary-regions"))) {
			// iterate over all necessary regions
			Object
				necRegions =
				this.getRegion().getFlag((ListFlag) flags.get(
					"necessary-regions"));
			if(necRegions instanceof List) {
				for(String regionName : (List<String>) this.getRegion().getFlag(
					(ListFlag) flags.get("necessary-regions"))) {
					// get the region
					CatchableArea
						region =
						AreaHandler.loadFromString(regionName);

					// check, if the region was found
					if(region != null) {
						// add the region to the list
						regions.add(region);
					}
				}
			}
		}

		// return the list
		return regions;
	}

	/**
	 * adds a {@link CatchableArea} to the list of necessary {@link
	 * CatchableArea}s
	 *
	 * @param area the {@link CatchableArea} to add
	 * @return <code>true</code>, if the {@link CatchableArea} successfully
	 * added
	 * @author ChrissW-R1
	 * @see ArrayList#add(Object)
	 * @since 1.0
	 */
	public boolean addNecessaryArea(CatchableArea area) {
		// check, if the region still exist
		if(this.getRegion() != null) {
			// check, if the catchable area to add is set
			// and if this area is already necessary for the area to set
			if(
				area != null
				&& !(this.isNecessaryFor(area))
			) {
				// create list of necessary regions as string
				ArrayList<String> stringList = new ArrayList<String>();

				// iterate over all stored necessary region
				for(CatchableArea region : this.getNecessaryAreas()) {
					// add the region string to the list
					stringList.add(AreaHandler.saveToString(region));
				}

				// add the new necessary region
				boolean
					success =
					stringList.add(AreaHandler.saveToString(area));

				// set the flag
				this.region.setFlag((ListFlag) (
					CatchMe.getFlags().get("necessary-regions")
				), stringList);

				// give feedback, if the new necessary region successfully added
				return success;
			}

			// return false, because the area is not set or this area is already necessary for the area to set
			return false;
		}

		// stop any active process
		this.stopAll();
		// return false, because the region doesn't exist
		return false;
	}

	/**
	 * removes a {@link CatchableArea} from the list of necessary {@link
	 * CatchableArea}s
	 *
	 * @param area the {@link CatchableArea} to remove
	 * @return <code>true</code>, if the {@link CatchableArea} was in the list
	 * @author ChrissW-R1
	 * @see ArrayList#remove(Object)
	 * @since 1.0
	 */
	public boolean removeNecessaryArea(CatchableArea area) {
		// check, if the region still exist
		if(this.getRegion() != null) {
			// check, if the catchable area to remove is set
			if(area != null) {
				// get the stored necessary regions
				ArrayList<CatchableArea> regionList = this.getNecessaryAreas();
				// remove the specified region from the list
				boolean success = regionList.remove(area);

				// check, if the region was successfully removed
				if(success) {
					// create list of necessary regions as strings
					ArrayList<String> stringList = new ArrayList<String>();

					// iterate over all necessary regions
					for(CatchableArea region : regionList) {
						// add the region string to the list
						stringList.add(AreaHandler.saveToString(region));
					}
				}

				// give feedback, if group successfully removed
				return success;
			}

			// return false, because an empty area couldn't be removed
			return false;
		}

		// stop any active process
		this.stopAll();
		// return false, because the region doesn't exist
		return false;
	}

	/**
	 * gives a list of all {@link Faction}s, which could occupy this {@link
	 * ProtectedRegion}
	 *
	 * @return the list of all {@link Faction}s, which could occupy this {@link
	 * ProtectedRegion}
	 * @author ChrissW-R1
	 * @since 0.9.1
	 */
	public List<Faction> getCatchgroups() {
		// create new list of all factions, which are allowed to conquer this region
		List<Faction> catchgroups = new ArrayList<>();

		Map<String, Flag<?>> flags = CatchMe.getFlags();

		// check, if the flag with the group names is set
		if(this.isFlagSet(flags.get("catchgroups"))) {
			// iterate over all group names
			Object groups = this.getRegion().getFlag((ListFlag) flags.get(
				"catchgroups"));
			if(groups instanceof List) {
				for(String factionTag : (List<String>) groups) {
					catchgroups.add(Factions.getInstance()
											.getByTag(factionTag));
				}
			}
		}

		// return the list
		return catchgroups;
	}

	/**
	 * adds a {@link Faction} to the list of possible conquerors
	 *
	 * @param faction the {@link Faction} to add
	 * @return <code>true</code>, if the {@link Faction} would successfully
	 * added
	 * @author ChrissW-R1
	 * @see ArrayList#add(Object)
	 * @since 0.9.1
	 */
	public boolean addCatchgroup(Faction faction) {
		// check, if the region still exist
		if(this.getRegion() != null) {
			// check, if the faction to add is set
			if(faction != null) {
				// get list with the current group names
				List<String> groupNames = CatchableArea.getFactionStringList(
					this.getCatchgroups());
				// add new group name
				boolean success = groupNames.add(faction.getTag());

				// save list in flag
				this.region.setFlag(
					(ListFlag) (CatchMe.getFlags().get("catchgroups")),
					groupNames
				);

				// give feedback, if the group successfully added
				return success;
			}

			// return false, because an empty faction couldn't be added
			return false;
		}

		// stop any active process
		this.stopAll();
		// return false, because the region doesn't exist
		return false;
	}

	/**
	 * removes a {@link Faction} from the list of possible conquerors
	 *
	 * @param faction the {@link Faction} to remove
	 * @return <code>true</code>, if the {@link Faction} was in the list
	 * @author ChrissW-R1
	 * @see ArrayList#remove(Object)
	 * @since 0.9.1
	 */
	public boolean removeCatchgroup(Faction faction) {
		// check, if the region still exist
		if(this.getRegion() != null) {
			// check, if the faction to remove is set
			if(faction != null) {
				// get list with the current group names
				List<Faction> catchgroups = this.getCatchgroups();
				// remove the given group
				boolean success = catchgroups.remove(faction);

				// save list in flag
				this.region.setFlag(
					(ListFlag) (CatchMe.getFlags().get("catchgroups")),
					CatchableArea.getFactionStringList(catchgroups)
				);

				// give feedback, if group successfully removed
				return success;
			}

			// return false, because an empty faction couldn't be removed
			return false;
		}

		// stop any active process
		this.stopAll();
		// return false, because the region doesn't exist
		return false;
	}

	/**
	 * gives the period
	 *
	 * @return the set period or the {@link CatchableArea#DEFAULT_PERIOD}, if
	 * none is set
	 * @author ChrissW-R1
	 * @since 0.9.1
	 */
	public int getPeriod() {
		// get the flag
		IntegerFlag flag =
			(IntegerFlag) (CatchMe.getFlags().get("catch-period"));

		// check, if the flag is set
		if(this.isFlagSet(flag)) {
			// get the flag value
			int value = this.getRegion().getFlag(flag);

			// check, if the flag value is valid
			if(value > 0) {
				// return flag value
				return value;
			}
		}

		// return default value
		return CatchableArea.DEFAULT_PERIOD;
	}

	/**
	 * sets the period
	 *
	 * @param period the period to set
	 * @author ChrissW-R1
	 * @since 0.9.1
	 */
	public void setPeriod(int period) {
		// check, if the region still exist
		if(this.getRegion() != null) {
			// check, if the value is valid
			if(period > 0) {
				// set the flag
				this.region.setFlag((IntegerFlag) (
					CatchMe.getFlags().get("catch-period")
				), period);
			}
		}
		// if the region doesn't exist any longer
		else {
			// stop any active process
			this.stopAll();
		}
	}

	/**
	 * gives the catching time
	 *
	 * @return the set catching time or the default {@link CatchableArea#DEFAULT_CATCHINGTIME},
	 * if none is set
	 * @author ChrissW-R1
	 * @since 0.9.1
	 */
	public int getCatchingTime() {
		// get the flag
		IntegerFlag
			flag =
			(IntegerFlag) (CatchMe.getFlags().get("catching-time"));

		// check, if the flag is set
		if(this.isFlagSet(flag)) {
			// get the flag value
			int value = this.getRegion().getFlag(flag);

			// check, if the flag value is valid
			if(value > 0) {
				// return flag value
				return value;
			}
		}

		// return default value
		return CatchableArea.DEFAULT_CATCHINGTIME;
	}

	/**
	 * sets the catching time
	 *
	 * @param catchingTime the catching time to set
	 * @author ChrissW-R1
	 * @since 0.9.1
	 */
	public void setCatchingTime(int catchingTime) {
		// check, if the region still exist
		if(this.getRegion() != null) {
			// check, if the value is valid
			if(catchingTime > 0) {
				// set the flag
				this.region.setFlag((IntegerFlag) (
					CatchMe.getFlags().get("catch-period")
				), catchingTime);
			}
		}
		// if the region doesn't exist any longer
		else {
			// stop any active process
			this.stopAll();
		}
	}

	/**
	 * gives the duration
	 *
	 * @return the set duration or the {@link CatchableArea#DEFAULT_DURATION},
	 * if none is set
	 * @author ChrissW-R1
	 * @since 0.9.1
	 */
	public int getDuration() {
		// get the flag
		IntegerFlag flag = (IntegerFlag) (
			CatchMe.getFlags().get("catch-duration")
		);

		// check, if the flag is set
		if(this.isFlagSet(flag)) {
			// get the flag value
			int value = this.getRegion().getFlag(flag);

			// check, if the flag value is valid
			if(value >= 0) {
				// return flag value
				return value;
			}
		}

		// return default value
		return CatchableArea.DEFAULT_DURATION;
	}

	/**
	 * sets the duration
	 *
	 * @param duration the duration to set
	 * @author ChrissW-R1
	 * @since 0.9.1
	 */
	public void setDuration(int duration) {
		// check, if the region still exist
		if(this.getRegion() != null) {
			// check, if the value is valid
			if(duration >= 0) {
				// set the flag
				this.region.setFlag((IntegerFlag) (
					CatchMe.getFlags().get("catch-duration")
				), duration);
			}
		}
		// if the region doesn't exist any longer
		else {
			// stop any active process
			this.stopAll();
		}
	}

	/**
	 * gives the money
	 *
	 * @return the set money or the {@link CatchableArea#DEFAULT_MONEY}, if none
	 * is set
	 * @author ChrissW-R1
	 * @since 0.9.1
	 */
	public double getMoney() {
		// get the flag
		DoubleFlag flag = (DoubleFlag) (CatchMe.getFlags().get("catch-money"));

		// check, if the flag is set
		if(this.isFlagSet(flag)) {
			// return flag value
			return this.getRegion().getFlag(flag);
		}

		// return default value
		return CatchableArea.DEFAULT_MONEY;
	}

	/**
	 * sets the money
	 *
	 * @param money the money to set
	 * @author ChrissW-R1
	 * @since 0.9.1
	 */
	public void setMoney(double money) {
		// check, if the region still exist
		if(this.getRegion() != null) {
			// set the flag
			this.region.setFlag(
				(DoubleFlag) (CatchMe.getFlags().get("catch-money")),
				money
			);
		}
		// if the region doesn't exist any longer
		else {
			// stop any active process
			this.stopAll();
		}
	}

	/**
	 * gives the {@link Faction}, which controls this {@link ProtectedRegion}
	 *
	 * @return the {@link Faction}, which controls this {@link
	 * ProtectedRegion}<br /> or <code>null</code>, if no {@link Faction} owned
	 * this {@link ProtectedRegion}
	 * @author ChrissW-R1
	 * @see CatchableArea#getControllingPlayer()
	 * @since 0.9.1
	 */
	public Faction getControllingFaction() {
		// check, if the region still exist
		if(this.getRegion() != null) {
			// check, if the region is flagged as free for all
			if(!(this.isFFA())) {
				// create variable of controlling groups
				Set<String> groups;
				// check, if the region is not flagged as save to member
				if(!(this.isMember())) {
					// get all controlling groups from owners
					groups = this.getRegion().getOwners().getGroups();
				}
				// if the region is flagged as save to member
				else {
					// get all controlling groups from members
					groups = this.getRegion().getMembers().getGroups();
				}

				// iterate over all group names, which are owners of this region
				for(String factionTag : groups) {
					// get faction from the group name
					Faction
						faction =
						Factions.getInstance().getByTag(factionTag);
					List<Faction> catchgroups = this.getCatchgroups();

					// check, if the faction is allowed to conquer and own this region
					if(
						faction != null
						&& (
							catchgroups.isEmpty() || catchgroups.contains(
								faction)
						)
					) {
						// return the first found faction, which owns this region
						return faction;
					}
				}
			}
		}
		// if the region doesn't exist any longer
		else {
			// stop any active process
			this.stopAll();
		}

		// return null, because no owner faction was found or the region is flagged to free for all
		return null;
	}

	/**
	 * gives the {@link Player}, which controls this {@link ProtectedRegion}
	 *
	 * @return the {@link Player}, which controls this {@link
	 * ProtectedRegion}<br /> or <code>null</code>, if no {@link Player} owned
	 * this {@link ProtectedRegion}
	 * @author ChrissW-R1
	 * @see CatchableArea#getControllingFaction()
	 * @since 1.0
	 */
	public Player getControllingPlayer() {
		// check, if the region still exist
		if(this.getRegion() != null) {
			// check, if the region is flagged to free for all
			if(this.isFFA()) {
				// get all player owners
				Object[]
					owners =
					this.getRegion().getOwners().getPlayers().toArray();
				// check, if the region has any player owner
				if(owners.length >= 1) {
					// return the first player owner
					return CatchMe.getInstance()
								  .getServer()
								  .getPlayer((String) (owners[0]));
				}
			}
		}
		// if the region doesn't exist any longer
		else {
			// stop any active process
			this.stopAll();
		}

		// return null, because no owner was found or the region is not flagged to free for all
		return null;
	}

	/**
	 * gives the {@link Faction} or {@link Player}, which currently controls
	 * this {@link ProtectedRegion}
	 *
	 * @return the {@link Faction} or {@link Player} (if the free-for-all-flag
	 * is set to <code>true</code>), which currently controls this {@link
	 * ProtectedRegion}
	 * @author ChrissW-R1
	 * @see CatchableArea#getControllingFaction()
	 * @see CatchableArea#getControllingPlayer()
	 * @since 1.0
	 */
	public Object getController() {
		// check, if this region is flagged to free for all
		if(this.isFFA()) {
			// return the controlling player
			return this.getControllingPlayer();
		}

		// return the controlling faction
		return this.getControllingFaction();
	}

	/**
	 * is this {@link CatchableArea} necessary to could conquer another {@link
	 * CatchableArea}
	 *
	 * @param area the {@link CatchableArea}, which this {@link CatchableArea}
	 *             could be necessary for
	 * @return <code>true</code>, if this {@link CatchableArea} is necessary for
	 * the other {@link CatchableArea}
	 * @author ChrissW-R1
	 * @since 1.0
	 */
	public boolean isNecessaryFor(CatchableArea area) {
		return area.getNecessaryAreas().contains(this);
	}

	/**
	 * gives all {@link CatchableArea}s, which this {@link CatchableArea} is
	 * necessary for
	 *
	 * @return the list of {@link CatchableArea}s, which this {@link
	 * CatchableArea} is necessary for
	 * @author ChrissW-R1
	 * @since 1.0
	 */
	public ArrayList<CatchableArea> getAreasNecessaryFor() {
		// create the list of all regions, which this region is necessary for
		ArrayList<CatchableArea> list = new ArrayList<CatchableArea>();

		// iterate over all worlds of the server
		for(World world : Bukkit.getWorlds()) {
			// iterate over all regions of the world
			for(ProtectedRegion region : WorldGuard.getInstance()
												   .getPlatform()
												   .getRegionContainer()
												   .get(BukkitAdapter.adapt(
													   world
												   ))
												   .getRegions()
												   .values()) {
				// check, if the region is catchable
				if(AreaHandler.isRegionCatchable(region)) {
					// get the catchable area
					CatchableArea iterArea = AreaHandler.getArea(world, region);
					// check, if this region is necessary for the other region
					if(this.isNecessaryFor(iterArea)) {
						// add the region to the list
						list.add(iterArea);
					}
				}
			}
		}

		// return the list of region, which this region is necessary for
		return list;
	}

	/**
	 * is a {@link Faction} allowed to conquer this {@link ProtectedRegion}
	 *
	 * @param faction the {@link Faction} to check
	 * @return <code>true</code>, if the {@link Faction} is allowed to conquer
	 * this {@link ProtectedRegion}
	 * @author ChrissW-R1
	 * @since 0.9.1
	 */
	public boolean isAllowedToConquer(Faction faction) {
		// check, if the faction is set
		if(faction != null) {
			// get the controlling faction of this region
			Faction controllingFaction = this.getControllingFaction();

			// iterate over all necessary regions
			for(CatchableArea area : this.getNecessaryAreas()) {
				// check, if the area is owned by someone
				if(area.getController() != null) {
					// check, if the region is flagged as free for all
					if(area.isFFA()) {
						// check, if the owner is a member of the conquering faction
						if(FPlayers.getInstance()
								   .getByPlayer(area.getControllingPlayer())
								   .getFaction() != faction) {
							// return false, because the owner is not in the conquering faction
							return false;
						}
					}
					// if the region is not flagged as free for all
					else {
						// get the faction, which controls the other faction
						Faction foreignFaction = area.getControllingFaction();
						// check, if the conqueror is the same to the region owner is allied to the owner
						if(
							foreignFaction != faction
							&& !(foreignFaction.getRelationTo(faction).isAlly())
						) {
							// return false, because the control of a necessary region is missing
							return false;
						}
					}
				}
				// if the region has no owner
				else {
					// return false, because the conquering faction doesn't control the region
					return false;
				}
			}

			// check, if the region is currently controlled
			if(controllingFaction != null) {
				// check if both factions are enemies
				if(
					controllingFaction != faction
					&& !(faction.getRelationTo(controllingFaction).isEnemy())
				) {
					// return false, because only enemies of the current controller could conquer the region
					return false;
				}
			}

			// get the list of possible conquerors
			List<Faction> catchgroups = this.getCatchgroups();
			// return, if the list of possible conquerors is empty
			// of if the faction is in the list of possible conquerors
			return (catchgroups.isEmpty() || catchgroups.contains(faction));
		}

		// return false, because a non set faction couldn't conquer a region
		return false;
	}

	/**
	 * is a {@link Player} allowed to conquer this {@link CatchableArea}
	 *
	 * @param player the {@link Player} to check
	 * @return <code>true</code>, if the {@link Player} is allowed to conquer
	 * this {@link CatchableArea}
	 * @author ChrissW-R1
	 * @since 1.0
	 */
	public boolean isAllowedToConquer(Player player) {
		// check, if the conquering player is set
		if(player != null) {
			// get the players faction
			Faction
				faction =
				FPlayers.getInstance().getByPlayer(player).getFaction();

			// iterate over all regions, which are necessary for this region
			for(CatchableArea area : this.getNecessaryAreas()) {
				// check, if the region is flagged as free for all
				if(area.isFFA()) {
					// check, if the player is the owner of the region
					if(area.getControllingPlayer() != player) {
						// return false, because the player need to control all necessary regions
						return false;
					}
				}
				// if region is not flagged as free for all
				else {
					// check, if the controller is the players faction
					if(area.getControllingFaction() != faction) {
						// return false, because the players faction need to control all necessary regions
						return false;
					}
				}
			}

			// return, if the list of possible conquerors is empty
			// and if not, if the players faction is allowed to conquer this region
			return (
				this.getCatchgroups().isEmpty()
				|| (faction != null && this.isAllowedToConquer(faction))
			);
		}

		// return false, because the player must exist to conquer a region
		return false;
	}

	/**
	 * gives a list of all {@link Faction}s, which are currently in this {@link
	 * ProtectedRegion}
	 *
	 * @return a list of all {@link Faction}s, which are currently in this
	 * {@link ProtectedRegion}
	 * @author ChrissW-R1
	 * @since 0.9.1
	 */
	public ArrayList<Faction> getAllFactionsHere() {
		// create a new list of factions
		ArrayList<Faction> factions = new ArrayList<Faction>();

		// iterate over all players, which are in this region
		for(Player player : this.getPlayers()) {
			// get the faction of the current player
			Faction
				faction =
				FPlayers.getInstance().getByPlayer(player).getFaction();

			// check, if the faction is already in the list
			// and if the faction is null
			if(
				!factions.contains(faction)
				&& faction != null
			) {
				// add the faction to the list
				factions.add(faction);
			}
		}

		// return the list with all faction, which are currently in this region
		return factions;
	}

	/**
	 * is one of the current controlling {@link Faction} or any allied {@link
	 * Faction} of it here?
	 *
	 * @return <code>true</code>, if any defender is currently in this {@link
	 * ProtectedRegion}
	 * @author ChrissW-R1
	 * @since 0.9.1
	 */
	public boolean isDefenderHere() {
		// get the list with all faction, which are in this region
		ArrayList<Faction> allFactionsHere = this.getAllFactionsHere();

		// check, if this region is controlled by a faction
		if(allFactionsHere != null) {
			// iterate over all factions, which are in this region
			for(Faction faction : allFactionsHere) {
				// get the faction, which controls this region
				Faction controllingFaction = this.getControllingFaction();

				// check, if the faction is the current controller or is allied with it
				if(
					controllingFaction != null
					&& (
						faction == controllingFaction
						|| controllingFaction.getRelationTo(faction).isAlly()
					)
				) {
					// return true, because the faction is a defender
					return true;
				}
			}
		}

		// return false, because no defender would found
		return false;
	}

	/**
	 * interacts, if a {@link Player} enter this {@link ProtectedRegion}
	 *
	 * @param player the {@link Player}, which enters the region
	 * @return <code>true</code>, if the {@link Player} is in the current
	 * control {@link Faction}<br /> or the {@link Player} starts to occupy this
	 * {@link ProtectedRegion}
	 * @author ChrissW-R1
	 * @since 0.9.1
	 */
	public boolean playerEnter(Player player) {
		// return value
		boolean conquerPlayer = false;

		// check, if the player is already known in this region
		if(!this.getPlayers().contains(player)) {
			// add the player to the list of all players, which are in this region
			this.players.add(player);

			// get the players faction
			Faction
				faction =
				FPlayers.getInstance().getByPlayer(player).getFaction();
			// get the faction, which controls this region
			Faction controllingFaction = this.getControllingFaction();
			// get the player, which controls this region
			Player controllingPlayer = this.getControllingPlayer();
			// get the name of this region
			String name = this.getName();
			// get the information about the free for all flag
			boolean ffa = this.isFFA();

			// create a additional part to the information message, if the region is already controlled
			String message = "";
			// create variable of the controller name
			String controller = "-";
			// check, if this region is controlled by someone
			if(this.getController() != null) {
				// set the additional part to the overall same phrase
				message = ChatColor.YELLOW + ", which is owned by ";

				// check, if this region is flagged as free for all
				if(ffa) {
					// get the player name as the controller name
					controller = controllingPlayer.getName();

					// check, if the player is the owner
					if(controllingPlayer == player) {
						// add the additional information
						message += "you";
						// set the controller name to green
						controller = ChatColor.GREEN + controller;
					}
					// if the player enter a foreign region
					else {
						// set the controller name to red
						controller = ChatColor.RED + controller;
						// set the additional information with the controller name
						message += "the player " + controller;
					}
				}
				// if the region is not flagged as free for all
				else {
					// get the tag of the current controller
					controller = controllingFaction.getTag();

					// check, if the player is one of the controllers
					if(controllingFaction == faction) {
						// set the message, which information that you are the owner
						message += ChatColor.GREEN + "your faction";

						// set the controller name to green
						controller = ChatColor.GREEN + controller;
					}
					// if the player is not in the controlling faction
					else {
						// get the relation of both factions
						Relation relation = controllingFaction.getRelationTo(
							faction);
						// check, if the factions are allied
						if(relation.isAlly()) {
							// set the controller name to green
							controller = ChatColor.GREEN + controller;
						}
						// check, if the factions are enemies
						else if(relation.isEnemy()) {
							// set the controller name to red
							controller = ChatColor.RED + controller;
						}
						// if the factions are neutral to each other
						else {
							// set the controller name to cyan
							controller = ChatColor.AQUA + controller;
						}

						// set the message, which information about the current owner
						message += "the faction " + controller;
					}
				}
			}

			// send an information message to the entering player, about its current region
			(
				new world.urelion.catchme.message.Message(
					ChatColor.YELLOW + name,
					true,
					controller,
					true,
					Material.MAP,
					ChatColor.YELLOW + "You entered the region "
					+ ChatColor.BLUE + name
					+ message
					+ ChatColor.YELLOW + "."
				)
			).send(player);

			// check, if this region is flagged as free for all
			if(ffa) {
				// check, if the player is the owner of the region
				if(player == controllingPlayer) {
					// stop catching and neutralization processes
					this.stopCatching();
				}
				// check, if the player is allowed to conquer this region
				else if(this.isAllowedToConquer(player)) {
					// create variable with new conqueror
					Player newConqueror = null;
					// iterate over all players in this region
					for(Player iterPlayer : this.getPlayers()) {
						// check, if the iterated player is allowed to conquer this region
						// and if the iterated player is the same as the entering player
						// and if the iterated player is the same as the current conqueror
						if(
							this.isAllowedToConquer(iterPlayer)
							&& iterPlayer != this.getConquerPlayer()
							&& iterPlayer != player
						) {
							// check, if a catching process is active
							if(this.isCatching()) {
								// stop the catching process
								this.stopCatching();
								// stop the looking for a new possible conqueror
								break;
							}

							// check, if another possible conqueror was found
							if(newConqueror == null) {
								// set the new conqueror
								newConqueror = iterPlayer;
							}
							// if another possible conqueror was found
							else {
								// reset the first possible conqueror
								newConqueror = null;
								// stop the looking for a new possible conqueror
								break;
							}
						}
					}

					// check, if a new conqueror was found
					if(newConqueror != null) {
						// start the catching process of the new conqueror
						this.startCatching(newConqueror);
					}
				}
			}
			// check, if the player has a faction
			else if(faction != null) {
				// check, if the player is in the current controlling faction
				// and if the player is in an allied faction
				if(
					faction != controllingFaction
					&& !(faction.getRelationTo(controllingFaction).isAlly())
				) {
					// checks, if no defender faction is in this region
					// and if the players faction is allowed to occupy this region
					if(
						!this.isDefenderHere()
						&& this.isAllowedToConquer(faction)
					) {
						// checks, if someone already occupy this region
						if(this.isCatching()) {
							// check, if the players faction is the same as the currently occupying one
							if(faction != this.getConquerFaction()) {
								// send an information message to all online players of the conquer faction
								this.getConquerFaction().sendMessage(
									ChatColor.YELLOW +
									"Your try to conquer the region "
									+
									ChatColor.BLUE +
									name
									+
									ChatColor.YELLOW +
									" failed, because the faction "
									+
									ChatColor.AQUA +
									faction.getTag()
									+
									ChatColor.YELLOW +
									" also tries it!"
								);

								// stop the catching process
								this.stopCatching();
							}
						}
						// if someone already occupy this region
						else {
							// start the catchingTimer to catch this region
							this.startCatching(faction);
						}
					}
				}
				// if the players faction is the currently controlling one
				else {
					// set to true, because the players faction is the currently controlling one
					conquerPlayer = true;

					// check, if someone tries to change the owner of this region
					if(this.getConquerFaction() != null) {
						String messageConqueror =
							"The region's owner faction "
							+ ChatColor.AQUA + controllingFaction.getTag();

						// check, if the player is in the owner faction
						if(faction == controllingFaction) {
							// send an information message to all online players of the owner faction
							controllingFaction.sendMessage(
								ChatColor.YELLOW +
								"Your faction defended your region "
								+
								ChatColor.BLUE +
								name
								+
								ChatColor.YELLOW +
								" from conquer of the faction "
								+
								ChatColor.AQUA +
								this.getConquerFaction().getTag()
								+
								ChatColor.YELLOW +
								"!"
							);

							// iterate over all factions
							for(Faction alliedFaction : Factions.getInstance()
																.getAllFactions()) {
								// check, if the faction is allied to the controlling faction
								if(controllingFaction.getRelationTo(
									alliedFaction).isAlly()) {
									// send an information message to the allied faction
									alliedFaction.sendMessage(
										ChatColor.YELLOW +
										"Your allied faction "
										+
										ChatColor.AQUA +
										controllingFaction.getTag()
										+
										ChatColor.YELLOW +
										" defended their the region "
										+
										ChatColor.BLUE +
										name
										+
										ChatColor.YELLOW +
										" against "
										+
										ChatColor.AQUA +
										this.getConquerFaction().getTag()
										+
										ChatColor.YELLOW +
										"!"
									);
								}
							}
						}
						// if the player is not in the owner faction
						else {
							// send an information message to all online players of the allied defender faction
							faction.sendMessage(
								ChatColor.YELLOW +
								"Your faction defend the region "
								+
								ChatColor.BLUE +
								name
								+
								ChatColor.YELLOW +
								" of your allied faction "
								+
								ChatColor.AQUA +
								controllingFaction.getTag()
								+
								ChatColor.YELLOW +
								" from conquer of the faction "
								+
								ChatColor.AQUA +
								this.getConquerFaction().getTag()
								+
								ChatColor.YELLOW +
								"!"
							);

							// send an information message to all online players of the owner faction
							controllingFaction.sendMessage(
								ChatColor.YELLOW +
								"Your allied faction "
								+
								ChatColor.GREEN +
								faction.getTag()
								+
								ChatColor.YELLOW +
								" defended your region "
								+
								ChatColor.BLUE +
								name
								+
								ChatColor.YELLOW +
								" from conquer of the faction "
								+
								ChatColor.RED +
								this.getConquerFaction().getTag()
								+
								ChatColor.YELLOW +
								"!"
							);

							messageConqueror =
								"The allied faction "
								+ ChatColor.AQUA + faction.getTag()
								+ ChatColor.YELLOW + " of the region's owner ";
						}

						// send an information message to all online players of the attacker faction
						this.getConquerFaction().sendMessage(
							ChatColor.YELLOW +
							messageConqueror
							+
							ChatColor.YELLOW +
							" foiled you try to conquer the region "
							+
							ChatColor.BLUE +
							name
							+
							ChatColor.YELLOW +
							"!"
						);

						// stop the catching process
						this.stopCatching();
					}
					// check, if the region proceed a neutralization
					else if(this.catchingTimer != null) {
						// stop the neutralization process
						this.stopCatching();
					}
				}
			}
		}

		// return, if the players faction is the currently controlling or the player start to occupy this region
		return conquerPlayer;
	}

	/**
	 * interacts, if a {@link Player} leaves this {@link ProtectedRegion}
	 *
	 * @param player the {@link Player}, which leaves this {@link
	 *               ProtectedRegion}
	 * @author ChrissW-R1
	 * @since 0.9.1
	 */
	public void playerLeave(Player player) {
		// check, if player is in this region
		if(this.getPlayers().contains(player)) {
			// get the faction of the player
			Faction
				faction =
				FPlayers.getInstance().getByPlayer(player).getFaction();
			// get the faction, which controls this region
			Faction controllingFaction = this.getControllingFaction();
			// get the player, which controls this region
			Player controllingPlayer = this.getControllingPlayer();
			// remove the player from the list of players in this region
			this.players.remove(player);

			// get the name of this region
			String name = this.getName();
			// get the information about the free for all flag
			boolean ffa = this.isFFA();

			// create string with the formated name of this region
			String formatName = name;

			// check, if this region is flagged as free for all
			if(ffa) {
				// check, if someone controls this region
				if(controllingPlayer != null) {
					// check, if the player is the owner of this region
					if(controllingPlayer == player) {
						// set the region name to green
						formatName = ChatColor.GREEN + formatName;
					}
					// if the player is not the owner of this region
					else {
						// set the region name to red
						formatName = ChatColor.RED + formatName;
					}
				}
			}
			// if the region is not flagged as free for all
			else {
				// check, if the player has a faction
				if(faction != null) {
					// create a variable of the relation of both factions
					Relation relation = Relation.NEUTRAL;

					// get the relation of both factions
					relation = faction.getRelationTo(controllingFaction);

					// check, if the player is one of the controllers
					// or if its faction is allied with the controlling faction
					if(
						faction == controllingFaction
						|| relation.isAlly()
					) {
						// set the region name to green
						formatName = ChatColor.GREEN + formatName;
					}
					// check, if the player is in an enemy faction of the controlling faction
					else if(relation.isEnemy()) {
						// set the region name to red
						formatName = ChatColor.RED + formatName;
					}
				}
			}

			// check, if the region name has any color
			if(!(formatName.startsWith("" + ChatColor.COLOR_CHAR))) {
				// set the region name to cyan for neutral status
				formatName = ChatColor.AQUA + formatName;
			}

			// send an information message to the leaving player
			(
				new world.urelion.catchme.message.Message(
					ChatColor.YELLOW + "You left",
					false,
					formatName,
					true,
					Material.MAP,
					ChatColor.YELLOW + "You left the region "
					+ formatName
					+ ChatColor.YELLOW + "."
				)
			).send(player);

			// check, if this region is flagged as free for all
			if(ffa) {
				// check, if the leaving player is the owner of this region
				if(player == controllingPlayer) {
					// check, if no player is in this region
					if(this.getPlayers().isEmpty()) {
						// start neutralization process
						this.startNeutralizing(false);
					}
				}
				// if the leaving player is not the owner of this region
				else {
					// check, if the leaving player was the conqueror
					if(this.getConquerPlayer() == player) {
						// stop the catching process
						this.stopCatching();

						// send an information message to the conqueror
						player.sendMessage(
							ChatColor.YELLOW +
							"You stopped to conquer the region "
							+
							ChatColor.BLUE +
							name
							+
							ChatColor.YELLOW +
							"!"
						);

						// check, if this region is controlled by someone
						if(controllingPlayer != null) {
							// send an information message to the regions owner
							controllingPlayer.sendMessage(
								ChatColor.AQUA +
								player.getName()
								+
								ChatColor.YELLOW +
								" has stopped to conquer your region "
								+
								ChatColor.BLUE +
								name
								+
								ChatColor.YELLOW +
								"!"
							);
						}
					}

					// check, if only one player is in this region
					if(this.getPlayers().size() == 1) {
						// start conquer process for the last player
						this.startCatching(player);
					}
				}
			}
			// check, if the player has a faction
			// and if the player is allowed to conquer the region
			// or, if the players faction is allied to the controlling one
			else if(
				faction != null
				&& (
					this.isAllowedToConquer(faction)
					|| faction.getRelationTo(controllingFaction).isAlly()
				)
			) {
				// get the list with all faction, which are in this region
				ArrayList<Faction> allFactionsHere = this.getAllFactionsHere();

				// check, if the player is in the faction, which currently occupy this region
				if(faction == this.getConquerFaction()) {
					// check, if another player of the conquer faction is in this region
					if(!(allFactionsHere.contains(this.getConquerFaction()))) {
						// send an information message to all online players of the conquer faction
						this.getConquerFaction().sendMessage(
							ChatColor.YELLOW +
							"Your faction has stopped to conquer the region "
							+
							ChatColor.BLUE +
							name
							+
							ChatColor.YELLOW +
							"!"
						);

						// check, if this area is controlled by someone
						if(controllingFaction != null) {
							// send an information message to all online players of the controlling faction
							controllingFaction.sendMessage(
								ChatColor.YELLOW +
								"The faction "
								+
								ChatColor.AQUA +
								this.getConquerFaction().getTag()
								+
								ChatColor.YELLOW +
								" has stopped to conquer your region "
								+
								ChatColor.BLUE +
								name
								+
								ChatColor.YELLOW +
								"!"
							);

							// iterate over all factions
							for(Faction alliedFaction : Factions.getInstance()
																.getAllFactions()) {
								// check, if the faction is allied to the controlling faction
								if(controllingFaction.getRelationTo(
									alliedFaction).isAlly()) {
									// send an information message to the allied faction
									faction.sendMessage(
										ChatColor.RED +
										"The faction "
										+
										ChatColor.AQUA +
										this.getConquerFaction().getTag()
										+
										ChatColor.RED +
										" has stopped to conquer your region "
										+
										ChatColor.BLUE +
										name
										+
										ChatColor.RED +
										", which is controlled by your allied "
										+
										ChatColor.AQUA +
										controllingFaction.getTag()
										+
										ChatColor.RED +
										"!"
									);
								}
							}
						}

						// stop catching, if no other player is in this region
						this.stopCatching();
					}
				}
				// check, if the players faction is the currently controlling
				else if(
					controllingFaction == null
					|| !(this.isDefenderHere())
				) {
					// set faction, which now starts to occupy this region
					Faction newConqueror = null;

					// iterate over all factions, which are in this region
					for(Faction iterFaction : allFactionsHere) {
						// check, if the faction is allowed to conquer this region
						if(this.isAllowedToConquer(iterFaction)) {
							// check, if already found a new conqueror
							if(newConqueror == null) {
								// set new conqueror, to the first found faction
								newConqueror = iterFaction;
							} else {
								// reset the new conqueror, if more than one is found
								newConqueror = null;
								// break the search process of a new conqueror
								break;
							}
						}
					}

					// check, if no new conqueror was found
					if(newConqueror != null) {
						// start new catching process
						this.startCatching(newConqueror);
					}
					// check, if the region is already controlled
					else if(controllingFaction != null) {
						// start neutralization
						this.startNeutralizing(false);
					}
				}
			}
		}
	}
}
