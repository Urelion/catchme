/**
 * package of the {@link com.sk89q.worldguard.protection.regions.ProtectedRegion}
 * handling
 */
package world.urelion.catchme.region;

import com.sk89q.worldguard.protection.flags.Flag;
import com.sk89q.worldguard.protection.flags.FlagContext;
import com.sk89q.worldguard.protection.flags.InvalidFlagFormat;
import com.sk89q.worldguard.protection.flags.RegionGroup;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * defines a class
 *
 * @author ChrissW-R1
 * @version 2.0.0
 * @since 0.9.1
 */
public class ListFlag<T>
	extends Flag<List<T>> {
	// attributes
	// class attributes

	/**
	 * the {@link Character}, which separates the group names
	 *
	 * @author ChrissW-R1
	 * @since 0.9.1
	 */
	public static final String SEPARATOR = ";";

	private final Flag<T> subFlag;

	// constructors

	/**
	 * constructor from superclass
	 *
	 * @param name    the name of the {@link Flag}
	 * @param subFlag the {@link Flag}, which is stored in the list
	 * @author ChrissW-R1
	 * @see Flag#Flag(String)
	 * @since 2.0.0
	 */
	public ListFlag(String name, Flag<T> subFlag) {
		// call the constructor of the super class
		super(name);
		this.subFlag = subFlag;
	}

	/**
	 * constructor from superclass
	 *
	 * @param name    the name of the {@link Flag}
	 * @param rg      the {@link RegionGroup}
	 * @param subFlag the {@link Flag}, which is stored in the list
	 * @author ChrissW-R1
	 * @see Flag#Flag(String, RegionGroup)
	 * @since 2.0.0
	 */
	public ListFlag(String name, RegionGroup rg, Flag<T> subFlag) {
		// call the constructor if the super class
		super(name, rg);
		this.subFlag = subFlag;
	}

	// methods
	// implemented methods

	/*
	 * (non-Javadoc)
	 * @see Flag#parseInput(FlagContext)
	 */
	@Override
	public List<T> parseInput(FlagContext context)
	throws InvalidFlagFormat {
		String  input = context.getUserInput();
		List<T> items = new ArrayList<>();
		if(!input.isEmpty()) {
			for(String item : input.split(ListFlag.SEPARATOR)) {
				items.add(subFlag.parseInput(context.copyWith(
					null,
					item,
					null
				)));
			}
		}

		return items;
	}

	/*
	 * (non-Javadoc)
	 * @see Flag#unmarshal(Object)
	 */
	@Override
	public List<T> unmarshal(Object obj) {
		if(obj instanceof Collection) {
			Collection<?> col   = (Collection<?>) obj;
			List<T>       items = new ArrayList<>();

			for(Object subObject : items) {
				T item = this.subFlag.unmarshal(subObject);

				if(item != null) {
					items.add(item);
				}
			}

			return items;
		} else {
			return null;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see Flag#marshal(Object)
	 */
	@Override
	public Object marshal(List<T> list) {
		List<Object> items = new ArrayList<>();
		for(T item : list) {
			items.add(this.subFlag.marshal(item));
		}

		return items;
	}
}
