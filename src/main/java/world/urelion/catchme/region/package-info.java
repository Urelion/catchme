/**
 * package of {@link com.sk89q.worldguard.protection.regions.ProtectedRegion} handling
 * 
 * @author ChrissW-R1
 * @version 1.0
 * @since 0.9.1
 */
package world.urelion.catchme.region;
