/**
 * package of the {@link ProtectedRegion} handling
 */
package world.urelion.catchme.region;

// imports
// Java imports

import com.sk89q.worldedit.bukkit.BukkitAdapter;
import com.sk89q.worldedit.math.BlockVector3;
import com.sk89q.worldguard.WorldGuard;
import com.sk89q.worldguard.protection.flags.StateFlag;
import com.sk89q.worldguard.protection.flags.StateFlag.State;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Server;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.Plugin;
import world.urelion.catchme.CatchMe;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * defines a handler, to interact with {@link CatchableArea}s
 *
 * @author ChrissW-R1
 * @version 1.0
 * @since 0.9.1
 */
public class AreaHandler {
	// attributes
	// class attributes

	/**
	 * list of all known {@link CatchableArea}s
	 *
	 * @author ChrissW-R1
	 * @since 0.9.1
	 */
	private static final ArrayList<CatchableArea>
										   areas   =
		new ArrayList<CatchableArea>();
	/**
	 * list of {@link Player}s, which are in a {@link CatchableArea}s
	 *
	 * @author ChrissW-R1
	 * @since 0.9.1
	 */
	private static final ArrayList<Player> players = new ArrayList<Player>();

	// constructors

	/**
	 * standard constructor
	 *
	 * @author ChrissW-R1
	 * @since 0.9.1
	 */
	public AreaHandler() {
		// call the constructor of the super class
		super();
	}

	// methods
	// getters and setters

	/**
	 * gives the list of all {@link Player}s, which are in a {@link
	 * CatchableArea}
	 *
	 * @return the list of all {@link Player}s, which are in a {@link
	 * CatchableArea}
	 * @author ChrissW-R1
	 * @since 0.9.1
	 */
	public static ArrayList<Player> getPlayers() {
		return AreaHandler.players;
	}

	/**
	 * adds a {@link Player} to the list of {@link Player}s, which are in a
	 * {@link CatchableArea}
	 *
	 * @param player the {@link Player} to add
	 * @return <code>true</code>, if the {@link Player} would be successfully
	 * added
	 * @author ChrissW-R1
	 * @see ArrayList#add(Object)
	 * @since 0.9.1
	 */
	public static boolean addPlayer(Player player) {
		return AreaHandler.players.add(player);
	}

	/**
	 * removes a {@link Player} from the list of {@link Player}s, which are in a
	 * {@link CatchableArea}
	 *
	 * @param player the {@link Player} to remove
	 * @return <code>true</code>, if the list contained the {@link Player}
	 * @author ChrissW-R1
	 * @see ArrayList#remove(Object)
	 * @since 0.9.1
	 */
	public static boolean removePlayer(Player player) {
		return AreaHandler.players.remove(player);
	}

	// normal methods

	/**
	 * converts a {@link Boolean} to the associated {@link State}
	 *
	 * @param bool the {@link Boolean} to convert
	 * @return the associated {@link State}
	 * @author ChrissW-R1
	 * @since 1.0
	 */
	public static State booleanToState(boolean bool) {
		// check, if the boolean is set to true
		if(bool) {
			// return the state value, which response to true
			return State.ALLOW;
		}

		// return the default state value
		return State.DENY;
	}

	/**
	 * checks, if a {@link ProtectedRegion} is catchable
	 *
	 * @param region the {@link ProtectedRegion} to check
	 * @return <code>true</code>, if in the {@link ProtectedRegion} the flag
	 * <code>catchable</code> is <code>true</code><br />
	 * or <code>false</code>, if the flag is set to <code>false</code> or
	 * cleared
	 * @author ChrissW-R1
	 * @since 0.9.1
	 */
	public static boolean isRegionCatchable(ProtectedRegion region) {
		// check, if the region exist
		if(region != null) {
			// return the value of the flag
			return region.getFlag((StateFlag) (
				CatchMe.getFlags()
					   .get("catchable")
			)) == State.ALLOW;
		}

		// return false, because a non-exist region isn't catchable
		return false;
	}


	/**
	 * gives a {@link CatchableArea} from a {@link String}
	 *
	 * @param string the {@link String}, with the necessary information<br>
	 *               Format: <code>Worldname:RegionID</code><br /> Example:
	 *               <code>myWorld:ConquerCity</code> &rArr; region ConquerCity
	 *               in myWorld
	 * @return the {@link CatchableArea}
	 * @author ChrissW-R1
	 * @since 1.0
	 */
	public static CatchableArea loadFromString(String string) {
		// split the string into the world and the region names
		String[] attributes = string.split(":");

		// check, if both names are set
		if(attributes.length >= 2) {
			// get world from its name
			World world = Bukkit.getWorld(attributes[0]);

			// check, if the world exist
			if(world != null) {
				// get the region from its name
				ProtectedRegion
					region =
					WorldGuard.getInstance()
							  .getPlatform()
							  .getRegionContainer()
							  .get(BukkitAdapter.adapt(world))
							  .getRegion(attributes[1]);

				// check, if the region is catchable
				if(AreaHandler.isRegionCatchable(region)) {
					// return the known region
					return AreaHandler.getArea(world, region);
				}
			}
		}

		// return null, because this region could not be found, or isn't catchable
		return null;
	}

	/**
	 * save a {@link CatchableArea} in a {@link String} to save it in files or
	 * send it through the network
	 *
	 * @param area the {@link CatchableArea} to convert
	 * @return the {@link String} of the {@link CatchableArea}
	 * @author ChrissW-R1
	 * @since 1.0
	 */
	public static String saveToString(CatchableArea area) {
		return area.getWorld().getName() + ":" + area.getRegion().getId();
	}

	/**
	 * gives the count of known {@link CatchableArea}s
	 *
	 * @return the count of known {@link CatchableArea}s
	 * @author ChrissW-R1
	 * @since 0.9.1
	 */
	public static int countAreas() {
		return AreaHandler.areas.size();
	}

	/**
	 * load all {@link CatchableArea}s on the {@link Server}
	 *
	 * @author ChrissW-R1
	 * @since 1.0
	 */
	public static void reloadAreas() {
		// clear the list
		AreaHandler.areas.clear();

		// iterate over all worlds on the server
		for(World world : Bukkit.getWorlds()) {
			// iterate over all regions in a world
			for(ProtectedRegion region : WorldGuard.getInstance()
												   .getPlatform()
												   .getRegionContainer()
												   .get(BukkitAdapter.adapt(
													   world
												   ))
												   .getRegions()
												   .values()) {
				// check, if the region is catchable
				if(AreaHandler.isRegionCatchable(region)) {
					// load the region as a catchable area to the list
					AreaHandler.getArea(world, region);
				}
			}
		}
	}

	/**
	 * gives the {@link CatchableArea} of the given region
	 *
	 * @param world  the {@link World}, in which the {@link ProtectedRegion} is
	 * @param region the {@link ProtectedRegion}
	 * @return the {@link CatchableArea} of the {@link ProtectedRegion}
	 * @author ChrissW-R1
	 * @since 0.9.1
	 */
	public static CatchableArea getArea(World world, ProtectedRegion region) {
		// iterate over all remembered catchable regions
		Iterator<CatchableArea> iter = AreaHandler.areas.iterator();
		while(iter.hasNext()) {
			// get the list item
			CatchableArea area = iter.next();
			// check, if it is not longer catchable
			if(
				area == null
				|| !(AreaHandler.isRegionCatchable(area.getRegion()))
			) {
				// stop any active processes
				area.stopAll();

				// remove the region from the list
				AreaHandler.areas.remove(area);
			}

			// check, if the catchable area is the searched region
			if(area.getWorld() == world && area.getRegion().equals(region)) {
				return area;
			}
		}

		// create a new catchable area, if nothing is found
		CatchableArea area = new CatchableArea(region, world);
		// add the new catchable area
		AreaHandler.areas.add(area);

		// return the new one
		return area;
	}

	/**
	 * Filter regions, which have set the given {@link State}
	 *
	 * @param regions   the {@link Set} of regions to filter
	 * @param stateFlag the {@link StateFlag} to check the {@link State} in
	 * @param state     the {@link State} to filter for
	 * @return a {@link Set} of filtered regions
	 */
	public static Set<ProtectedRegion> getRegionsWithState(
		Set<ProtectedRegion> regions,
		StateFlag stateFlag,
		State state
	) {
		Set<ProtectedRegion> filteredRegions = new HashSet<>();

		for(ProtectedRegion region : regions) {
			if(region.getFlag(stateFlag).equals(state)) {
				filteredRegions.add(region);
			}
		}

		return filteredRegions;
	}

	/**
	 * checks, if a {@link Player} are in a {@link CatchableArea}
	 *
	 * @param event the {@link PlayerEvent}
	 * @return <code>true</code>, if the player is now in any {@link
	 * CatchableArea}
	 * @author ChrissW-R1
	 * @since 0.9.1
	 */
	public static boolean playerCatchingLookup(PlayerEvent event) {
		// get used objects
		Player   player = event.getPlayer();
		Location loc    = player.getLocation();
		RegionManager regionManager =
			WorldGuard.getInstance().getPlatform().getRegionContainer().get(
				BukkitAdapter.adapt(loc.getWorld())
			);

		// list of all catchable region, in which the player is
		ArrayList<CatchableArea> areas = new ArrayList<>();

		// check, if the flag catchable is set at the location of the player
		if(regionManager.getApplicableRegions(BlockVector3.at(
			loc.getX(),
			loc.getY(),
			loc.getZ()
		)).testState(
			null,
			(StateFlag) (CatchMe.getFlags().get("catchable"))
		)) {
			// get the count of regions, in which the player is
			int
				regionCounter =
				regionManager.getRegionCountOfPlayer(CatchMe.getWorldGuard()
															.wrapPlayer(player));

			// iterate over all WorldGuard regions in the world of the player
			for(ProtectedRegion region : regionManager.getRegions().values()) {
				// check, if the region contains the players location
				if(region.contains(
					loc.getBlockX(),
					loc.getBlockY(),
					loc.getBlockZ()
				)) {
					// check, if the region is catchable
					if(AreaHandler.isRegionCatchable(region)) {
						// get the catchable area from the region
						CatchableArea
							nextArea =
							AreaHandler.getArea(loc.getWorld(), region);
						// set the first found area in which the player is
						areas.add(nextArea);

						// checks, if the player is already in the area
						if(!(nextArea.getPlayers().contains(player))) {
							// let the player enter the region
							AreaHandler.playerEnteringArea(player, nextArea);
							// break the loop, because only one catchable region should be proceed per one step
							break;
						}
					}

					// decrease the counter
					regionCounter--;

					// check, if there is any other region, in which the player is
					if(regionCounter <= 0) {
						// break the loop, because the player is in no more region
						break;
					}
				}
			}
		}

		// remove the player from all catchable regions, it leaved
		AreaHandler.playerLeavingArea(player, areas);

		// check, if the player is in any region
		return AreaHandler.getPlayers().contains(player);
	}

	/**
	 * adds a {@link Player} to the list of {@link Player}s in {@link
	 * CatchableArea}s
	 *
	 * @param player the {@link Player} to add
	 * @param area   the {@link CatchableArea}, which the {@link Player}
	 *               entered
	 * @return <code>true</code> if the {@link Player} would successfully added
	 * or was already in the list
	 * @author ChrissW-R1
	 * @since 0.9.1
	 */
	public static boolean playerEnteringArea(
		Player player,
		CatchableArea area
	) {
		// let player enter the region and remember the result
		boolean conqueror = area.playerEnter(player);

		// check, if the player is in any region
		if(!AreaHandler.getPlayers().contains(player)) {
			// add the player to list of controlled players and return the full result
			return (AreaHandler.addPlayer(player) && conqueror);
		} else {
			// player is already in a region
			return true;
		}
	}

	/**
	 * removes a {@link Player} from the list of {@link Player}, which are in
	 * {@link CatchableArea}s
	 *
	 * @param player the {@link Player} to remove
	 * @param areas  the list of {@link CatchableArea}s, which the {@link
	 *               Player} should <b>not</b> leave
	 * @return <code>true</code>, if the {@link Player} would removed
	 * successfully
	 * @author ChrissW-R1
	 * @since 0.9.1
	 */
	public static boolean playerLeavingArea(
		Player player,
		ArrayList<CatchableArea> areas
	) {
		// checks, if the player is in any region
		if(AreaHandler.getPlayers().contains(player)) {
			// iterates over all regions in the world of the player
			for(ProtectedRegion region : WorldGuard.getInstance()
												   .getPlatform()
												   .getRegionContainer()
												   .get(BukkitAdapter.adapt(
													   player.getWorld()
												   ))
												   .getRegions()
												   .values()) {
				// check, if the region is catchable
				if(AreaHandler.isRegionCatchable(region)) {
					// load the region as catchable
					CatchableArea area = AreaHandler.getArea(
						player.getWorld(),
						region
					);

					// check, if the player should be leave all regions
					// and if the player should be leave this region
					if(
						areas == null
						|| !(areas.contains(area))
					) {
						// remove the player from this catchable region
						area.playerLeave(player);
					}
				}
			}

			// checks, if no region is given
			if(areas == null) {
				// remove the player from the list
				return AreaHandler.removePlayer(player);
			}

			// return true, because the player is in a region
			return true;
		}

		// return false, because the player is in no region
		return false;
	}

	/**
	 * handles a {@link Player}, who logged off
	 *
	 * @param event the {@link PlayerQuitEvent}
	 * @return <code>true</code> if the {@link Player} would successfully
	 * removed from the list
	 * @author ChrissW-R1
	 * @since 0.9.1
	 */
	public static boolean playerQuitArea(PlayerQuitEvent event) {
		return AreaHandler.playerLeavingArea(event.getPlayer(), null);
	}

	/**
	 * set up all {@link CatchableArea}s to disable the {@link Plugin}<br />
	 * <b>Attention</b>: Only for calling from the plugin main class!
	 *
	 * @author ChrissW-R1
	 * @since 1.0
	 */
	public void onDisable() {
		// iterate over all known catchable areas
		while(!AreaHandler.areas.isEmpty()) {
			// get the first item of the list
			CatchableArea area = AreaHandler.areas.get(0);
			// stop all active processes
			area.stopAll();
			// remove the catchable area from the list
			AreaHandler.areas.remove(area);
		}
	}
}
